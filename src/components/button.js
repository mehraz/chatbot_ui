import React from 'react';
import PropTypes from 'prop-types';

const Button = ({onClick, disabled, children}) => {
    const isDisabled = disabled || false;
    return (
        <button className="am-button am-button--primary" disabled={isDisabled} onClick={onClick}>{children}</button>
    );
};

Button.propTypes = {
    onClick: PropTypes.func.isRequired,
    disabled: PropTypes.bool
};

export default Button;

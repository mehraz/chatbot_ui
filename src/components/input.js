import React from 'react';
import PropTypes from 'prop-types';

const Input = ({name, label, value, onChange, inputType}) => {
    const type = inputType || 'text';
    return (
        <div className="am-form-group">
            <div className="am-input-text">
                <input id={name} className="am-input-text__field" type={type} name={name} placeholder={label} value={value} onChange={onChange} />
            </div>
        </div>
    );
};

Input.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func,
    inputType: PropTypes.string
};

export default Input;

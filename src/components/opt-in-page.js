import React from 'react';
import Input from './input.js';
import Button from './button.js';

let divStyle = {
  color: 'black',
  width: '1080px',
};


export const OptInPageView = () => (


<div>
    <div style={divStyle}>
            <textarea name="chat" cols="150" rows="20"/>
            <Input name="msg" label="say something" inputType="text"  onKeyPress= {(event) => handleKeyDown(event)} />
            <Button onClick={() => saveOptInFlag()} disabled={false}>send</Button>
    </div>
</div>
);



function handleKeyDown(e) {
    alert("yes")
    if (e.key === 'Enter' && e.shiftKey === false) {
      e.preventDefault();
      saveOptInFlag();
    }
  };


function saveOptInFlag() {
    var chatBox = document.getElementsByName("chat")[0];
    var msgBox = document.getElementsByName("msg")[0];
    var msgValue = msgBox.value;
    chatBox.value +=  "\nMe: " + msgBox.value;
    msgBox.value = "";
    return fetch('https://aqueous-river-78463.herokuapp.com/chatone/talk', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({query: msgValue})
    })
        .then(response => response.json())
        .then(x => {
            if (x.status.errorType === "success") {
            chatBox.value += "\nBotOne: " + x.result.fulfillment.speech;
            x.result.fulfillment.speech
        } else {
            chatBox.value += "\nBotOne: " + "some error.";
            "some error :("
        }
    }).catch(err => {
        throw new Error(err)
      })

}

//export default OptInPageView

import React from 'react';
import '../css/App.css';

const OptInSuccessView = ()=> {
    return (
        <div>
            Opt in Success - no further action
        </div>
    )
};

export default OptInSuccessView;
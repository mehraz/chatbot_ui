import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';
import { Provider } from 'react-redux';
import configureStore, { history } from './store/configureStore';
import rootReducer from './reducers';
import { ConnectedRouter } from 'react-router-redux';

const store = configureStore(rootReducer);

ReactDOM.render(
    <Provider store={ store }>
    <ConnectedRouter history={history}>
    <div>
      <App />
     </div>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);

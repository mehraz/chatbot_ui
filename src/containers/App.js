import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import OptInPageContainer from './opt-in-page-container';
import { updateEmailAction } from '../actions/email';
import OptInSuccessView from '../components/opt-in-success';
import { connect } from 'react-redux';

class App extends Component {
    render() {
        return (
            <div>
                <Route exact path="/" component={OptInPageContainer}/>
            </div>
        );
    }
}



export default connect(
  (state) => ({ router: state.router }), {
  onLoadData: updateEmailAction,
})(App);


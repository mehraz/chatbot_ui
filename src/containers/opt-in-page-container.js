import { connect } from 'react-redux';
import { updateEmailAction } from '../actions/email';
import { OptInPageView } from '../components/opt-in-page';
import { createSelector } from 'reselect';


const mapStateToProps = (state) => {
  return {
    robots: state,
    query: state.query,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    onLoadData: () => dispatch(updateEmailAction()),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OptInPageView);